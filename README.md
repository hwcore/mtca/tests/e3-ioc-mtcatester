# e3-ioc-mtcatester

This IOC is intended to run on a MTCA system under tests. It assumes that the MTCA system contains the following AMC cards:

- Concurrent Tech MTCA CPU (host running this IOC)
- MRF MTCA-EVR-300 
- Struck SIS8300 (FPGA Firmware: Generic Data Acquisition)
- IOxOS IFC14x0 (FPGA Firmware: EVR Sniffer)

## Description

The function of this IOC is to provide high level access (via EPICS) to the most important MTCA features:
- PCIe enumeration (CPU detecting other endpoints - EVR, SIS8300, IFC14x0)
- PCIe large data transfers (between the CPU and the SIS8300)
- MLVDS backplane lines (EVR driving and IFC14x0 detecting the pulses)

With this IOC running, any EPICS client can coordinate extensive test routines to validate any MTCA system before installation on site.

