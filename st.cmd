#- common E3 modules
require busy
require calc
require essioc

#- EVR modules
require mrfioc2

#- Struck sis8300 modules
require asyn
require adcore
require admisc
require adsis8300

#- IFC14x0 EVR sniffer
require evrsnifferioc

#- ----------------------------------------------------------------------------
#- General Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("SYSSUB",       "LabS-ICS")
epicsEnvSet("EVR_DEV",      "Ctrl-EVR-142")
epicsEnvSet("STRUCK_DEV",   "Ctrl-AMC-142")
epicsEnvSet("IFC14x0_DEV",  "Ctrl-AMC-242")
epicsEnvSet("IOCDEV",       "SC-IOC-042")

#- ----------------------------------------------------------------------------
#- essioc Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("IOCNAME",       "$(SYSSUB):$(IOCDEV)")
epicsEnvSet("IOCDIR",        "$(SYSSUB)_$(IOCDEV)")
#epicsEnvSet("AS_TOP",        "/tmp")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- ----------------------------------------------------------------------------
#- EVR Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("PEVR","$(SYSSUB):$(EVR_DEV)")
epicsEnvSet("EVRDLYGENARGS","W0=10,E0T0=14,W1=10,E1T0=14,W2=10,E2T0=14")
epicsEnvSet("PCIID","0e:00.0")
iocshLoad("$(E3_CMD_TOP)/iocsh/evr.iocsh")

#- ----------------------------------------------------------------------------
#- Struck SIS8300 Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("CONTROL_GROUP", "$(SYSSUB)")
epicsEnvSet("AMC_NAME",      "$(STRUCK_DEV)")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",      "$(EVR_DEV):")
epicsEnvSet("PREFIX",        "$(SYSSUB):$(AMC_NAME):")
iocshLoad("$(E3_CMD_TOP)/iocsh/sis8300.iocsh")

#- ----------------------------------------------------------------------------
#- IFC14x0 Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("IFC14x0_PCI",    "/dev/xdma0")
iocshLoad("$(E3_CMD_TOP)/iocsh/ifc14x0.iocsh")

#- ----------------------------------------------------------------------------
#- IOC Start
#- ----------------------------------------------------------------------------
iocInit()

